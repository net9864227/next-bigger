﻿using System;

#pragma warning disable

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer and null if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int? NextBiggerThan(int number)
        {
           if (number < 0)
            {
                throw new ArgumentException(string.Empty, nameof(number));
            }

        char[] num = number.ToString().ToCharArray();
            bool exist = false;
            int pos = num.Length - 2;
        for (int i = num.Length - 1; i > 0; i--)
            {
                if (num[i] > num[i-1])
                {
                    exist = true;
                    pos = i - 1;
                    break;
                }
            }

        if(!exist || number == int.MaxValue)
            {
                return null;
            }

            int change = pos + 1;
            for (int i = pos + 1; i < num.Length; i++)
            {
                if (num[i] > num[pos] && num[i] < num[change])
                {
                    change = i;
                }
            }

            char t = num[change];
            num[change] = num[pos];
            num[pos] = t;

            bool sorted = false;
            while(!sorted)
            {
                sorted = true;
                for (int i = pos + 1; i < num.Length-1; i++)
                {
                    if (num[i] > num[i + 1])
                    {
                        t = num[i];
                        num[i] = num[i + 1];
                        num[i + 1] = t;
                        sorted = false;
                    }
                }
            }
            int res = 0;
            for (int i = 0; i < num.Length; i++)
            {
                res *= 10;
                res += (int)num[i] - 48;
            }
         
            return res;

        }

    }
}
